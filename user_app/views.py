from rest_framework import viewsets

from user_app.filters import ProfileFilter
from user_app.models import Profile
from user_app.serializers import ProfileDetailsSerializer
from user_app.serializers import ProfileSerializer


class ProfileViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows profile.
    """

    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    detail_serializer_class = ProfileDetailsSerializer
    filterset_class = ProfileFilter

    def get_serializer_class(self):
        """default function to specify the serializer"""
        if self.action == "retrieve":
            return self.detail_serializer_class
        return super().get_serializer_class()
