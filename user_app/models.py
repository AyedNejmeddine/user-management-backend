from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

# genre options
MALE_OPTION = "male"
FEMALE_OPTION = "female"

GENDER_OPTIONS = (
    (MALE_OPTION, _("Male")),
    (FEMALE_OPTION, _("Female")),
)


class Profile(models.Model):
    """
    Profile a single model represented the user,
    related to model:`auth.User`.
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    home_town = models.CharField(max_length=50, null=False, blank=False)
    age = models.IntegerField(
        null=False,
        blank=False,
        validators=[MaxValueValidator(100), MinValueValidator(1)],
    )
    gender = models.CharField(
        max_length=10, choices=GENDER_OPTIONS, default=MALE_OPTION
    )

    def __str__(self):
        """
        Override this method to format profile object.
        """
        return f"{self.user.first_name} {self.user.last_name}"
