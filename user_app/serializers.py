from django.contrib.auth.models import User

from rest_framework import serializers

from user_app.models import Profile


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer for User Model.
    """

    class Meta:
        """Meta class to specify the model and fields of User."""

        model = User
        fields = ("first_name", "last_name")


class UserDetailsSerializer(UserSerializer):
    """
    Serializer for User details Model.
    This Serializer inherits from User Serializer.
    """

    class Meta(UserSerializer.Meta):
        """Meta class to specify the model and fields of User."""

        fields = UserSerializer.Meta.fields + ("username", "email", "date_joined")


class ProfileSerializer(serializers.ModelSerializer):
    """
    Serializer for Profile Model.
    """

    user = UserSerializer(many=False)

    class Meta:
        """Meta class to specify the model and fields of Profile."""

        model = Profile
        fields = ("id", "home_town", "age", "gender", "user")


class ProfileDetailsSerializer(ProfileSerializer):
    """
    Serializer for Profile details Model.
    This Serializer inherits from Profile Serializer.
    """

    user = UserDetailsSerializer(many=False)
