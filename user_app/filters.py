from django_filters.rest_framework import CharFilter
from django_filters.rest_framework import FilterSet
from django_filters.rest_framework import NumberFilter

from user_app.models import Profile


class ProfileFilter(FilterSet):
    """
    A filterset class to filter the profile with given fields on params.
    """

    age_greater = NumberFilter(field_name="age", lookup_expr="gte")
    age_less = NumberFilter(field_name="age", lookup_expr="lte")
    home_town = CharFilter(field_name="home_town", lookup_expr="contains")
    gender = CharFilter(field_name="gender", lookup_expr="exact")

    class Meta:
        model = Profile
        fields = ["age_greater", "age_less", "home_town", "gender"]
