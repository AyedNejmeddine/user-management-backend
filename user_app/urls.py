from rest_framework import routers

from user_app.views import ProfileViewSet

ROUTER = routers.DefaultRouter()
ROUTER.register(r"profiles", ProfileViewSet)


urlpatterns = ROUTER.urls
